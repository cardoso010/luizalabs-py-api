echo "------- Criando os containers -------"
docker-compose up -d
echo "------- Ambiente -------"
python3 -m venv .desafio_luizalabs
source .desafio_luizalabs/bin/activate
echo "------- Instalando as dependencias -------"
pip3 install -r requirements.txt