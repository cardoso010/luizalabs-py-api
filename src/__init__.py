import os

from flask import Flask
from flask_restful import Resource, Api
from flask_migrate import Migrate
from flask_swagger_ui import get_swaggerui_blueprint

from src.controllers.client_controller import Client, ClientList
from src.router import config_router
from src.models import db, ma

config = {
    'development': 'config.Development',
    'test': 'config.Test'
}

SWAGGER_URL = '/api/docs'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Desafio Luiza Labs"
    }
)


def create_app(config_env="development"):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    api = Api(app, prefix="/api/v1")
    app.config.from_object(config[config_env])
    db.init_app(app)
    ma.init_app(app)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    with app.app_context():
        config_router(api)
        app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
        db.create_all()
        # Create tables for our models
        return app
