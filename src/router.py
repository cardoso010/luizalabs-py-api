from src.controllers.client_controller import Client, ClientList, ClientFavorite
from src.controllers.product_controller import Product, ProductList, ProductInsertData


def config_router(api):
    # Client
    api.add_resource(Client, "/clients/<client_id>")
    api.add_resource(ClientList, "/clients/")
    api.add_resource(
        ClientFavorite, "/clients/<client_id>/favoriteproducts/")

    # Product
    api.add_resource(Product, "/products/<product_id>")
    api.add_resource(ProductList, "/products/")
    api.add_resource(ProductInsertData, "/products/insertdatabase")
