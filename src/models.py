from flask_sqlalchemy import SQLAlchemy, Pagination
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow

ma = Marshmallow()
db = SQLAlchemy()


favorite_product = db.Table('favorite_products',
                            db.Column('client_id', db.Integer, db.ForeignKey(
                                'clients.id'), primary_key=True),
                            db.Column('product_id', db.Integer, db.ForeignKey(
                                'products.id'), primary_key=True)
                            )


class Client(db.Model):
    __tablename__ = "clients"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    favorite_products = db.relationship(
        'Product', secondary=favorite_product, lazy='subquery', backref=db.backref('clients', lazy=True))

    def __init__(self, name, email):
        self.name = name
        self.email = email

    def __repr__(self):
        return '<Client %d>' % self.id


class Product(db.Model):
    __tablename__ = "products"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    brand = db.Column(db.String, nullable=False)
    image = db.Column(db.String, nullable=False)
    price = db.Column(db.Float, nullable=False)
    review_score = db.Column(db.Float)

    def __init__(self, title, brand, image, price, review_score=None):
        self.title = title
        self.brand = brand
        self.image = image
        self.price = price
        self.review_score = review_score

    def __repr__(self):
        return '<Product %d>' % self.id


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)

    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User %d>' % self.id


class ClientSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String(required=True)
    email = fields.String(required=True)

    class Meta:
        model = Client


class ProductSchema(ma.Schema):
    id = fields.Integer()
    title = fields.String(required=True)
    brand = fields.String(required=True)
    image = fields.String(required=True)
    price = fields.Float(required=True)
    review_score = fields.Float()

    class Meta:
        model = Product


class UserSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String(required=True)
    email = fields.String(required=True, validate=validate.Email(
        error="Email não é valido!"))
    password = fields.String(required=True)

    class Meta:
        model = User


class PaginationSchema(ma.Schema):
    class Meta:
        model = Pagination
