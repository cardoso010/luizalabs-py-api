from src.models import db, Client
from src.repositories.product_repository import ProductRepository


class ClientRepository:
    @staticmethod
    def all():
        return Client.query.all()

    @staticmethod
    def get_by_id(id):
        return Client.query.get_or_404(id)

    @staticmethod
    def create(data):
        client = None
        try:
            client = Client(data.name, data.email)
            db.session.add(client)
            db.session.commit()
        except Exception as error:
            print(error)
        return client

    @staticmethod
    def update(id, data):
        client = None
        try:
            client = Client.query.get(id)
            client.name = data.name
            client.email = data.email
            db.session.commit()
        except Exception as error:
            print(error)
        return client

    @staticmethod
    def delete(id):
        result = False
        try:
            Client.query.filter(Client.id == id).delete()
            db.session.commit()
            result = True
        except Exception as error:
            print(error)
        return result

    @staticmethod
    def add_favorite_product(client_id, product_id):
        result = None
        try:
            client = Client.query.get_or_404(client_id)
            product = ProductRepository.get_by_id(product_id)
            client.favorite_products.append(product)
            db.session.commit()
            result = True
        except Exception as error:
            print(error)
        return result

    @staticmethod
    def all_favorite_product(client_id):
        client = Client.query.get_or_404(client_id)
        return client.favorite_products
