from src.models import db, Product


class ProductRepository:
    @staticmethod
    def all(page):
        return Product.query.paginate(page=page, per_page=100).items

    @staticmethod
    def get_by_id(id):
        return Product.query.get_or_404(id)

    @staticmethod
    def create(data):
        product = None
        try:
            product = Product(data["title"], data["brand"],
                              data["image"], data["price"],
                              data.get("reviewScore", None))
            db.session.add(product)
            db.session.commit()
        except Exception as error:
            print(error)
        return product

    @staticmethod
    def update(id, data):
        product = None
        try:
            product = Product.query.get(id)
            product.title = data.title
            product.brand = data.brand
            product.image = data.image
            product.price = data.price
            product.review_score = data.review_score
            db.session.commit()
        except Exception as error:
            print(error)
        return product

    @staticmethod
    def delete(id):
        result = False
        try:
            Product.query.filter(Product.id == id).delete()
            db.session.commit()
            result = True
        except Exception as error:
            print(error)
        return result
