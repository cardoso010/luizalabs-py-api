from flask_restful import Resource
from src.repositories.product_repository import ProductRepository
from src.models import ProductSchema
from src.services.product_service import ProductService
from src.validators.product_validator import create_validator


class Product(Resource):
    def get(self, product_id):
        product = ProductRepository.get_by_id(product_id)
        return ProductSchema().dump(product)


class ProductList(Resource):
    def get(self):
        parser = create_validator()
        args = parser.parse_args()
        products = ProductRepository.all(args.page)
        return ProductSchema(many=True).dump(products)


class ProductInsertData(Resource):
    def get(self):
        ProductService.get_products()
        return {"status": "sucess"}, 200
