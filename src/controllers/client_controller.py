from flask_restful import Resource
from src.validators.client_validator import create_validator, create_favoriteproducts_validator
from src.repositories.client_repository import ClientRepository
from src.models import ClientSchema, ProductSchema


class Client(Resource):
    def get(self, client_id):
        """Return client by id"""
        client = ClientRepository.get_by_id(client_id)
        return ClientSchema().dump(client)

    def delete(self, client_id):
        """Delete client by id"""
        result = ClientRepository.delete(client_id)
        return {'status': result}, 200

    def put(self, client_id):
        """Update client"""
        parser = create_validator()
        args = parser.parse_args()
        client = ClientRepository.update(client_id, args)
        return ClientSchema().dump(client), 204


class ClientList(Resource):
    def get(self):
        """Return list of client"""
        clients = ClientRepository.all()
        return ClientSchema(many=True).dump(clients)

    def post(self):
        """Creates a new client"""
        parser = create_validator()
        args = parser.parse_args()
        client = ClientRepository.create(args)
        return ClientSchema().dump(client), 201


class ClientFavorite(Resource):
    def get(self, client_id):
        """Return a list of product favorites"""
        favorite_products = ClientRepository.all_favorite_product(client_id)
        return ProductSchema(many=True).dump(favorite_products)

    def post(self, client_id):
        """Add a product favorites to client"""
        parser = create_favoriteproducts_validator()
        args = parser.parse_args()
        result = ClientRepository.add_favorite_product(
            client_id, args.product_id)
        return {'status': result}, 201
