import requests
from src.repositories.product_repository import ProductRepository


class ProductService:
    @staticmethod
    def insert_products(products):
        try:
            for product in products:
                ProductRepository.create(product)
        except Exception as error:
            print(error)

    @staticmethod
    def get_products():
        url = "http://challenge-api.luizalabs.com/api/product/"
        try:
            for page in [1, 2, 3]:
                r = requests.get(url, {"page": page})
                data = r.json()
                ProductService.insert_products(data["products"])
        except Exception as error:
            print(error)
