from flask_restful import reqparse


def create_validator():
    parser = reqparse.RequestParser()
    parser.add_argument('name', type=str, required=True,
                        help='Nome é obrigatorio!')
    parser.add_argument('email', type=str, required=True,
                        help='Email é obrigatorio!')
    return parser


def create_favoriteproducts_validator():
    parser = reqparse.RequestParser()
    parser.add_argument('product_id', type=int, required=True,
                        help='Produto é obrigatorio!')
    return parser
