from flask_restful import reqparse


def create_validator():
    parser = reqparse.RequestParser()
    parser.add_argument('email', help='Email é obrigatorio!')
    parser.add_argument('password', help='Senha é obrigatorio!')
    args = parser.parse_args()
    return args
