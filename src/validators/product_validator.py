from flask_restful import reqparse


def create_validator():
    parser = reqparse.RequestParser()
    parser.add_argument('page', type=int, required=True,
                        help='Pagina é obrigatorio!')
    # args = parser.parse_args()
    return parser
