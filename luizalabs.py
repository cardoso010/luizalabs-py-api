from src import create_app
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand


if __name__ == '__main__':
    app = create_app()
    app.run()
