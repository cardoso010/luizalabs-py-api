import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = b'\xce\x11\xbaJ\r=\x08\x19\xee-\xb0k0.\xbf\xbc'
    FLASK_APP = 'luizalabs.py'
    FLASK_ENV = 'development'
    DEBUG = False
    TESTING = False
    DB_SERVER = 'localhost'

    @property
    def DATABASE_URI(self):         # Note: all caps
        return 'postgresql://postgres:postgres@{}/desafio'.format(self.DB_SERVER)


class Development(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost/desafio'
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class Test(Config):
    FLASK_ENV = 'test'
    TESTING = True
    DATABASE_URI = 'sqlite:///:memory:'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
