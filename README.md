# Desafio Luiza Labs

## Requisitos

```
- Python 3.7.3
- docker
- docker-compose
```

## Getting Started

Clonar repositório

```
$git clone https://cardoso010@bitbucket.org/cardoso010/luizalabs-py-api.git
```

Acessar o diretorio

Criação dos conteiners e execução do projeto

```
$docker-compose up -d
```

Criar o ambiente env e acessar

```
$python3 -m venv .desafio_luizalabs
$source .desafio_luizalabs/bin/activate
```

Instalar as dependencias

```
$pip3 install -r requirements.txt
```

Acessar o banco postgres pelo pgadmin ou por outro client que desejar e criar um banco chamado "desafio"

Apos criar o banco, executar os comando para rodar as migrations

```
$python3 migrate.py db migrate
```

Executando o proejto

```
$flask run
```

O projeto irá rodar na seguinte url e porta

```
http://localhost:5000/api/v1/
```

A documentação da api está rodando na seguinte url

```
http://localhost:5000/api/docs/
```

#### pgadmin4

Para acessar de forma mais facil o banco de dados basta acessar a url `http://localhost:16543` usando o usuario `local@local.dev` e a senha `postgres` que foi definida no .env.
Ã‰ necessario criaÃ§Ã£o de um server e adicionar os dados de acesso ao banco que estÃ¡ no .env.

## Autor

- **Gabriel Cardoso Luiz** - _Initial work_ - [cardoso010](https://github.com/cardoso010)
